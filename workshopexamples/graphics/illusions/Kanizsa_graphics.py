from graphics import *
window = GraphWin("Kanisza", 400,400)
window.setCoords(-200,-200,200,200)
window.setBackground('white')

centers = (
	Point(-100,-100),
	Point(-100,100),
	Point(100,100),
	Point(100,-100),
)

for center in centers:
	disk = Circle(center, 80)
	disk.setOutline('black')
	disk.setFill('black')
	disk.draw(window)

square = Rectangle(Point(-100,-100),Point(100,100))
square.setOutline('white')
square.setFill('white')
square.draw(window)

window.getMouse() # Pause to view result
