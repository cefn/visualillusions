from graphics import *
from math import radians, sin, cos
from time import sleep

window = GraphWin("BreathingSquare", 400, 400, autoflush=False)
window.setCoords(-200, -200, 200, 200)
window.setBackground('white')

diameter = 200.0
radius = diameter / 2.0
distance = radius + 20


def getCirclePoints(angles, radius, rotate=0):
    corners = []
    for deg in angles:
        rad = radians(deg + rotate)
        corners.append(Point(sin(rad) * radius, cos(rad) * radius))
    return corners


def createSquareCorners(rotate=0):
    return getCirclePoints(angles=[-45, 45, 135, 225], radius=100, rotate=rotate)


mover = Polygon(*createSquareCorners(rotate=0))
mover.setFill('red')
mover.setOutline('red')
mover.draw(window)

for xposition in [distance, -distance]:
    for yposition in [distance, -distance]:
        square = Rectangle(Point(-radius, -radius), Point(radius, radius))
        square.move(xposition, yposition)
        square.setFill('black')
        square.setOutline('black')
        square.draw(window)

while True:
    for angle in range(0, 360, 2):
        mover.points = createSquareCorners(rotate=angle)
        sleep(0.05)
        window.redraw()