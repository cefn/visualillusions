import pip
sys.__stdout__ = sys.stdout
command = "install graphics.py"
pip.main(command.split())

# note this line in pip/compat.py as per https://github.com/pypa/pip/commit/d3f040e1201f36bcbdcb8e41f341180a677fd772 to fix https://github.com/pypa/pip/issues/3356
# output_encoding = getattr(getattr(sys, "__stderr__", None), encoding", None)
