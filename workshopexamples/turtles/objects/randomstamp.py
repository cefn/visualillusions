from turtle import *
from random import randrange

screen = Screen()

tim = Turtle()
tim.register_shape('square', ((0,0),(1,0),(1,1),(0,1)))
tim.shape('square')
tim.fillcolor((255,0,0))

def bounce():
    while True:
        tim.goto(randrange(-200, 200), randrange(-200,200))
        tim.stamp()