import math, numpy, random #handy system and math functions
from psychopy import core, event, visual, gui #these are the psychopy modules

myWin = visual.Window(color='white', units='pix', size=[1000,1000], allowGUI=False, fullscr=False)#creates a window 
myClock = core.Clock() #this creates and starts a clock which we can later read

lineLeft =visual.Line(myWin, start=[-160,-340], end=[-160,200], lineColor='black',  lineWidth=5, ori=5)
lineRight =visual.Line(myWin, start=[160,-340], end=[160,200], lineColor='black',  lineWidth=5, ori=-5)
rectangle =visual.Rect(myWin, width=180, height=30, pos =[0,-160], fillColor='grey', lineColor=None)

myScale = visual.RatingScale(myWin, pos=[0, -360], low=0, high=45,  textSize=0.5, lineColor='black',  tickHeight=False, scale=None, showAccept=False, singleClick=True)
information=visual.TextStim(myWin, pos=[0,-385], text='', height=18, color='black') 

# the main loop
def mainLoop():
    
    finished = False
    while not finished:
        
        lineLeft.draw()
        lineRight.draw()
    
        rectangle.setPos([0,-160])
        rectangle.draw()
        rectangle.setPos([0,140])
        rectangle.draw()
    
        myScale.draw()
        information.draw()
        myWin.flip()
        
        if myScale.noResponse ==False: #some new value has been selected with the mouse
            orientation = myScale.getRating()
            lineLeft.setOri(orientation)
            lineRight.setOri(-orientation)
            information.setText(str(orientation))
            myScale.reset()
    
        if event.getKeys(keyList=['escape']): #pressing ESC quits the program
            finished =True
    
mainLoop() #enters the main loop
myWin.close() #closes the window
core.quit() #quits