import math, numpy, random #handy system and math functions
from psychopy import core, event, visual, gui #these are the psychopy modules

myWin = visual.Window(color='white', units='pix', size=[1000,1000], allowGUI=False, fullscr=False)#creates a window 
myClock = core.Clock() #this creates and starts a clock which we can later read

grayRectangleLeft = visual.Rect(myWin, width=100, height=25, fillColor=[0,0,0], lineColor=None)
grayRectangleRight = visual.Rect(myWin, width=100, height=25, fillColor=[0,0,0], lineColor=None)
blackBar = visual.Rect(myWin, width=600, height=25, fillColor='black', lineColor=None)
blackSquare = visual.Rect(myWin, width=300, height=400, pos=[-150,0],fillColor='black', lineColor=None)
whiteSquare = visual.Rect(myWin, width=300, height=400, pos=[150,0],fillColor='white', lineColor='black')

myScale = visual.RatingScale(myWin, pos=[0, -360], textSize=0.4, lineColor='black',  tickHeight=False, scale=None, choices=[-.28,-.24,-.20,-.16,-.12,-.08,-.04,0,.04,.08,.12,.16,.20,.24,.28], stretch =2.0, showAccept=False, singleClick=True)
information=visual.TextStim(myWin, pos=[0,-385], text='', height=18, color='black') 
title=visual.TextStim(myWin, pos=[0,305], text='', height=24, color='green') 

#there are 4 possible states in terms of what is on screen
def switchStatesAndDrawBackground(barHeight, showIllusion, illusionName):
    
        if showIllusion==True and illusionName=="White":
            title.setText('White illusion')
            for index in range(-6,7,2):
                blackBar.setPos([0, barHeight * index])
                blackBar.draw()
        elif showIllusion==True and illusionName=="SBC":
            title.setText('Simultaneous Brightness Contrast')
            blackSquare.draw()
            whiteSquare.draw()
        elif showIllusion==False and illusionName=="White":
            title.setText('White illusion - OFF')
        elif showIllusion==False and illusionName=="SBC":
            title.setText('Simultaneous Brightness Contrast - OFF')

#six grey rectangles, three on the left three on the right 
def drawRectangles(barHeight):
    
    xPos = 120
    
    grayRectangleLeft.setPos([-xPos, barHeight * -3])
    grayRectangleLeft.draw()
    grayRectangleLeft.setPos([-xPos, barHeight * -1])
    grayRectangleLeft.draw()
    grayRectangleLeft.setPos([-xPos, barHeight * 1])
    grayRectangleLeft.draw()
    
    grayRectangleRight.setPos([xPos, barHeight * -2])
    grayRectangleRight.draw()
    grayRectangleRight.setPos([xPos, barHeight * 0])
    grayRectangleRight.draw()
    grayRectangleRight.setPos([xPos, barHeight * 2])
    grayRectangleRight.draw()

# the main loop
def mainLoop():
    
    finished = False
    illusionName = "White"
    showIllusion = True
    barHeight =25
    grayRectangleLeft.setHeight(barHeight)
    grayRectangleRight.setHeight(barHeight)
    blackBar.setHeight(barHeight)
    
    while not finished:
        
        switchStatesAndDrawBackground(barHeight, showIllusion, illusionName)
        drawRectangles(barHeight)
        
        information.draw()
        myScale.draw()
        title.draw()
        myWin.flip()
        
        if myScale.noResponse ==False: #some new value has been selected with the mouse
            gray = myScale.getRating()
            grayRectangleLeft.setFillColor(gray)
            grayRectangleRight.setFillColor(-gray)
            information.setText(str(gray))
            myScale.reset()
    
        pressedList =event.getKeys(keyList=['escape','a','s']) #pressing ESC quits the program
        if len(pressedList) >0:
            if pressedList[0] =='escape':
                finished =True
            elif pressedList[0] =='a':
                if illusionName =='White': illusionName='SBC'
                elif illusionName =='SBC': illusionName='White'
            elif pressedList[0] =='s':
                showIllusion = not showIllusion
            event.clearEvents()


mainLoop() #enters the main loop
myWin.close() #closes the window
core.quit() #quits





