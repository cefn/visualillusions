import math, numpy, random #handy system and math functions
from psychopy import core, event, visual, gui #these are the psychopy modules

myWin = visual.Window(color='white', units='pix', size=[1000,1000], allowStencil=True, allowGUI=False, fullscr=False)#creates a window 
myClock = core.Clock() #this creates and starts a clock which we can later read

line =visual.Line(myWin, start=[-200,0], end=[200,0])
myStencil =visual.Aperture(myWin, size=100, shape='circle')
myScale = visual.RatingScale(myWin, pos=[0, -360], low=1, high=400,  textSize=0.5,lineColor='black', tickHeight=False, scale=None, showAccept=False, singleClick=True)
information=visual.TextStim(myWin, pos=[0,-385], text='', height=18, color='black') 
title=visual.TextStim(myWin, pos=[0,305], text='Neon Colour Spreading', height=24, color='green') 
    
# pick randomly N vertical positions on the left and N vertical positions on the right
def chooseYPositions(n):
    
    yLeft =[]
    yRight =[]
    for index in range(n):
        yLeft.append( random.randint(-200,200))
        yRight.append( random.randint(-200,200))
        
    return yLeft, yRight

# draw N lines that go from -200 to 200 horizontally, and from yLeft to yRight vertically
def drawLines(yLeft, yRight, n): 

    for index in range(n):
        line.start= [-200, yLeft[index]]
        line.end = [200, yRight[index]]
        line.draw()

# the main loop
def mainLoop(number =24):
    
    finished = False
    yLeft, yRight = chooseYPositions(number)
    myStencil.enabled = True 
    showIllusion =True

    while not finished:

        myStencil.inverted= False
        line.setLineColor('red')
        line.setLineWidth(2)
        drawLines(yLeft,yRight, number)
        
        myStencil.inverted= True
        line.setLineColor('black')
        line.setLineWidth(1)
        if showIllusion ==True: 
            drawLines(yLeft,yRight, number)
        
        title.draw()
        information.draw()
        myScale.draw()
        myWin.flip()
        
        if myScale.noResponse ==False:
            c = myScale.getRating()
            information.setText(str(c))
            myStencil.size =c
            myScale.reset()
    
        pressedList =event.getKeys(keyList=['escape','a','s']) #pressing ESC quits the program
        if len(pressedList) >0:
            if pressedList[0] =='escape':
                finished =True
            elif pressedList[0] =='a':
                yLeft, yRight = chooseYPositions(number)
            elif pressedList[0] =='s':
                showIllusion = not showIllusion
            event.clearEvents()

    myStencil.enabled = False

mainLoop() #enters the main loop
myWin.close() #closes the window
core.quit() #quits





