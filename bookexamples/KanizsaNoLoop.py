import math, numpy, random #to have handy system and math functions 
from psychopy import core, event, visual, gui #these are the psychopy libraries

myWin = visual.Window(color='white', units='pix', size=[1000,1000], allowGUI=False, fullscr=False) #creates a window
myClock = core.Clock() #this creates and starts a clock which we can later read

disk = visual.Circle(myWin, radius=80, fillColor='black', lineColor=None)
square =visual.Rect(myWin, width=200, height=200, fillColor='white', lineColor=None)

disk.setPos([-100,-100])
disk.draw()
disk.setPos([-100,100])
disk.draw()
disk.setPos([100,100])
disk.draw()
disk.setPos([100,-100])
disk.draw()

square.draw()
   
myWin.flip()

core.wait(5)  #waits for 5 seconds
myWin.close() #closes the window
core.quit() #quits



