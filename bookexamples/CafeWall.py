import math, numpy, random #handy system and math functions
from psychopy import core, event, visual, gui #these are the psychopy modules

myWin = visual.Window(color='white', units='pix', size=[1000,1000], allowGUI=False, fullscr=False)#creates a window 
myClock = core.Clock() #this creates and starts a clock which we can later read

blackBar = visual.Rect(myWin, width=50, height=10, fillColor='black', lineColor=None)
mortar = visual.Rect(myWin, width=50, height=2, fillColor=[0.2,0.2,0.2], lineColor=None)

myScale = visual.RatingScale(myWin, pos=[0, -360], low=0, high=50,  textSize=0.5, lineColor='black',  tickHeight=False, scale=None, showAccept=False, singleClick=True)
information=visual.TextStim(myWin, pos=[0,-385], text='', height=18, color='black') 
title=visual.TextStim(myWin, pos=[0,305], text='Cafe Wall Illusion', height=24, color='green') 

#black rectangles drawn in columns, shifting every other to the right by shift 
def drawColumns(shift, barHeight, barWidth): 

    startX = - barWidth * 6
    endX =  barWidth * 6
    startY = - barHeight * 4
    endY =  barHeight * 4
    for x in range(startX, endX+1, barWidth*2):
        oddRow = False
        for y in range(startY, endY+1, barHeight):
            if oddRow == True:
              blackBar.setPos([x+shift,y])
              oddRow =False
            else:
              blackBar.setPos([x,y])
              oddRow=True
            blackBar.draw()

#draw the mortar lines
def drawMortar(barHeight):
    
    startY = - barHeight * 5 + barHeight/2
    endY =  barHeight * 5 -barHeight/2
    for y in range(startY, endY+1, barHeight):
        mortar.setPos([0,y])
        mortar.draw()
        
# the main loop
def mainLoop():
    
    finished = False
    illusionName = 'Cafe Wall illusion'
    barHeight =40
    barWidth =50
    shift = barWidth /2
    blackBar.setHeight(barHeight)
    blackBar.setWidth(barWidth)
    mortar.setWidth(barWidth*14)
    
    while not finished:
        
        drawColumns(shift, barHeight, barWidth)
        drawMortar(barHeight)
        title.setText(illusionName)
        information.draw()
        myScale.draw()
        title.draw()
        myWin.flip()
        
        if myScale.noResponse ==False: #some new value has been selected with the mouse
            shift = myScale.getRating()
            information.setText(str(shift))
            myScale.reset()
    
        pressedList =event.getKeys(keyList=['escape','a']) #pressing ESC quits the program
        if len(pressedList) >0:
            if pressedList[0] =='escape':
                finished =True
            elif pressedList[0] =='a':
                if illusionName == 'Cafe Wall illusion': 
                  illusionName='Munsterberg illusion'
                  mortar.setFillColor('black')
                elif illusionName == 'Munsterberg illusion': 
                  illusionName='Cafe Wall illusion'
                  mortar.setFillColor('grey')
            event.clearEvents()

mainLoop() #enters the main loop
myWin.close() #closes the window
core.quit() #quits





